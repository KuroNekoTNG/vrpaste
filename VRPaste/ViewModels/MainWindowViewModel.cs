﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;
using ReactiveUI;
using WindowsInput;
using WindowsInput.Native;

namespace VRPaste.ViewModels {
	public class MainWindowViewModel : ViewModelBase {
		[DllImport( "user32.dll" )]
		private static extern IntPtr SetForegroundWindow( IntPtr hWnd );

		[DllImport( "user32.dll" )]
		private static extern bool ShowWindow( IntPtr hWnd, int nCmdShow );

		private static readonly VirtualKeyCode[] MODIFIER = new[] { VirtualKeyCode.CONTROL, };

		public ICommand PasteIntoVrChat { get; }

		private readonly InputSimulator simulator;

		public MainWindowViewModel() {
			simulator = new InputSimulator();

			PasteIntoVrChat = ReactiveCommand.Create( PasteIntoVrc );
		}

		private void PasteIntoVrc() {
			var vrc = Process.GetProcessesByName( "VRChat" ).FirstOrDefault();

			if ( vrc == null ) {
				return;
			}
			
			

			SetForegroundWindow( vrc.MainWindowHandle );
			ShowWindow( vrc.MainWindowHandle, 3 );

			simulator.Keyboard.ModifiedKeyStroke( MODIFIER, VirtualKeyCode.VK_V );
			simulator.Keyboard.KeyPress( VirtualKeyCode.RETURN );
		}
	}
}
